import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { CreateUserDto } from '../user/dto/create-user.dto';

import { UserService } from '../user/user.service';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService,
  ) { }

  async validateUser(username: string, hashedPassword: string): Promise<any> {
    // find if user with this email exists (email is passed as the username)
    const user = await this.userService.findOneByEmail(username);

    if (!user) {
      return null;
    }

    // find if user password match
    const match = await this.comparePassword(hashedPassword, user.password);
    if (!match) {
      return null;
    }

    // tslint:disable-next-line: no-string-literal
    const { password, ...result } = user['dataValues']; // removing password from the retrieved user object
    return result;
  }

  public async login(user: any): Promise<{ accessToken: string }> {
    // generating accesss token
    const tokenPayload = {
      id: user.id,
      username: user.username,
      isAdmin: user.isAdmin,
    };
    const accessToken = await this.generateToken(tokenPayload);
    
    const responseBody = {
      accessToken,
    }

    return responseBody;
  }

  public async create(user: CreateUserDto) {
    // hash the password
    const hashedPassword = await this.hashPassword(user.password);

    // create the user, replacing the raw password with the hashed version
    const newUser = await this.userService.create({ ...user, password: hashedPassword });

    // tslint:disable-next-line: no-string-literal
    const { password, ...result } = newUser['dataValues'];

    return result;
  }

  private async generateToken(user: any): Promise<string> {
    const token = await this.jwtService.signAsync(user, {
      secret: this.configService.get<string>('ACCESS_TOKEN_SECRET'),
      expiresIn: this.configService.get<string>('ACCESS_TOKEN_EXPIRY_TIMESPAN'),
    });
    return token;
  }

  private async hashPassword(password: any) {
    const hash = await bcrypt.hash(password, 10);
    return hash;
  }

  private async comparePassword(enteredPassword: string, dbPassword: string): Promise<boolean> {
    const match = await bcrypt.compare(enteredPassword, dbPassword);
    return match;
  }
}
