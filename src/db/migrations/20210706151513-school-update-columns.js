'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await Promise.all([
      queryInterface.removeColumn('Schools', 'password'),
      queryInterface.removeColumn('Schools', 'isAdmin'),
      queryInterface.addColumn('Schools', 'createdAt', {
        type: Sequelize.DATE,
        allowNull: false,
      }),
      queryInterface.addColumn('Schools', 'updatedAt', {
        type: Sequelize.DATE,
        allowNull: false,
      }),
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    await Promise.all([
      queryInterface.addColumn('Schools', 'password', {
        type: Sequelize.STRING,
      }),
      queryInterface.addColumn('Schools', 'isAdmin', {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      }),
      queryInterface.removeColumn('Schools', 'createdAt'),
      queryInterface.removeColumn('Schools', 'updatedAt'),
    ]);
  },
};
