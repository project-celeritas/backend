import { 
  Controller, 
  Get, 
  Post, 
  Body, 
  Patch, 
  Param, 
  Delete, 
  UseGuards, 
  NotFoundException,
  Req,
 } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { FormService } from './form.service';
import { CreateFormDto, UpdateFormDto } from './dto';
import { FindOneParams } from 'src/utils/dto/find-one-params';
import { UserSchoolExistsGuard } from '../_core/guards/user-school-exists.guard';
import { StreamExistGuard } from '../_core/guards/streams-exist.guard';
import { UniqueSchoolFormGuard } from '../_core/guards/unique-school-form.guard';

@Controller('forms')
export class FormController {
  constructor(private readonly formService: FormService) {}

  @UseGuards(
    AuthGuard('jwt'), 
    UserSchoolExistsGuard,
    UniqueSchoolFormGuard,
    StreamExistGuard,
  )
  @Post()
  async create(@Body() createFormDto: CreateFormDto) {
    return this.formService.create(createFormDto);
  }

  @UseGuards(
    AuthGuard('jwt'),
    UserSchoolExistsGuard,
  )
  @Get()
  async findAll(@Req() req) {
    return this.formService.findAllBySchoolId(req.user.schoolId);
  }

  @UseGuards(
    AuthGuard('jwt'),
    UserSchoolExistsGuard,
  )
  @Get(':id')
  async findOne(@Param() params: FindOneParams, @Req() req) {
    const form = await this.formService.findOne(params.id);

    if (!form) {
      throw new NotFoundException(`Form with id: ${params.id} does not exist`);
    }

    return form;
  }

  @UseGuards(
    AuthGuard('jwt'),
    UserSchoolExistsGuard,
  )
  @Patch(':id')
  async update(@Param() params: FindOneParams, @Req() req, @Body() updateFormDto: UpdateFormDto) {
    return this.formService.update(params.id, updateFormDto);
  }

  @UseGuards(AuthGuard('jwt'))
  @Delete(':id')
  async remove(@Param() params: FindOneParams, @Req() req) {
    return this.formService.remove(params.id);
  }
}
