import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { FormTeacherService } from './form-teacher.service';
import { CreateFormTeacherDto } from './dto/create-form-teacher.dto';
import { UpdateFormTeacherDto } from './dto/update-form-teacher.dto';

@Controller('form-teachers')
export class FormTeacherController {
  constructor(private readonly formTeacherService: FormTeacherService) {}

  @Post()
  create(@Body() createFormTeacherDto: CreateFormTeacherDto) {
    return this.formTeacherService.create(createFormTeacherDto);
  }

  @Get()
  findAll() {
    return this.formTeacherService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.formTeacherService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateFormTeacherDto: UpdateFormTeacherDto) {
    return this.formTeacherService.update(+id, updateFormTeacherDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.formTeacherService.remove(+id);
  }
}
