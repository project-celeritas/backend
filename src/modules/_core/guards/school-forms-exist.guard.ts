import { CanActivate, ExecutionContext, Injectable, ForbiddenException, NotFoundException } from '@nestjs/common';
import { Observable } from 'rxjs';
import { FormService } from 'src/modules/form/form.service';

@Injectable()
export class SchoolFormsExistGuard implements CanActivate {
  constructor(private readonly formService: FormService) { }

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    return this.validateRequest(request);
  }

  async validateRequest(request) {
    const userSchoolId = request.body.schoolId;
    const formIds = [...new Set<string>(request.body.formIds)]; // to remove duplicates

    if (formIds) {
      for (const formId of formIds) {
        const foundForm = await this.formService.findOneByIdAndSchoolId(Number(formId), userSchoolId);
        if (!foundForm) {
          throw new NotFoundException(`Form with id: ${formId} does not exist in your school`);
        }
      }
    }

    return true;
  }
}