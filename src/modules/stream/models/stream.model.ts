import { BelongsTo, BelongsToMany, Column, DataType, ForeignKey, Model, Table } from "sequelize-typescript";
import { FormStream } from "src/modules/form-stream/models/form-stream.model";

import { Form } from "src/modules/form/models/form.model";
import { School } from "src/modules/school/models/school.model";

@Table
export class Stream extends Model {
  @Column({
    type: DataType.STRING,
  })
  name: string;

  /* model associations */
  @ForeignKey(() => School)
  @Column({
    type: DataType.INTEGER,
  })
  schoolId: number;

  @BelongsTo(() => School, 'schoolId')
  school: School;

  @BelongsToMany(() => Form, () => FormStream, 'formId')
  forms: Array<Form & {FormStream: FormStream}>;
}
