import { BelongsTo, BelongsToMany, Column, DataType, ForeignKey, Model, Table } from 'sequelize-typescript';

import { School } from 'src/modules/school/models/school.model';
import { SubjectTeacher } from 'src/modules/subject-teacher/models/subject-teacher.model';
import { Subject } from 'src/modules/subject/models/subject.model';
import { FormTeacher } from 'src/modules/form-teacher/models/form-teacher.model';
import { Form } from 'src/modules/form/models/form.model';

@Table
export class Teacher extends Model {
  @Column({
    type: DataType.STRING,
  })
  name: string;

  @Column({
    type: DataType.DATE,
  })
  createdAt: Date;

  @Column({
    type: DataType.DATE,
  })
  updatedAt: Date;

  /* model associations */
  @ForeignKey(() => School)
  @Column({
    type: DataType.INTEGER,
  })
  schoolId: number;

  @BelongsTo(() => School, 'schoolId')
  school: School;

  @BelongsToMany(() => Subject, () => SubjectTeacher, 'subjectId')
  subjects: Array<Subject & {SubjectTeacher: SubjectTeacher}>;

  @BelongsToMany(() => Form, () => FormTeacher, 'formId')
  forms: Array<Form & {FormTeacher: FormTeacher}>;
}
