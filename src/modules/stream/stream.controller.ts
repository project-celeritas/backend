import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, NotFoundException, Req } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { StreamService } from './stream.service';
import { CreateStreamDto, UpdateStreamDto } from './dto';
import { FindOneParams } from 'src/utils/dto/find-one-params';
import { UserSchoolExistsGuard } from '../_core/guards/user-school-exists.guard';
import { UniqueSchoolStreamGuard } from '../_core/guards/unique-school-stream.guard';

@Controller('streams')
export class StreamController {
  constructor(private readonly streamService: StreamService) { }

  @UseGuards(
    AuthGuard('jwt'),
    UserSchoolExistsGuard,
    UniqueSchoolStreamGuard,
  )
  @Post()
  async create(@Body() createStreamDto: CreateStreamDto) {
    return await this.streamService.create(createStreamDto);
  }

  @UseGuards(
    AuthGuard('jwt'),
    UserSchoolExistsGuard,
  )
  @Get()
  async findAll(@Req() req) {
    return await this.streamService.findAllBySchoolId(req.user.schoolId);
  }

  @UseGuards(
    AuthGuard('jwt'),
    UserSchoolExistsGuard,
  )
  @Get(':id')
  async findOne(@Param() params: FindOneParams, @Req() req) {
    const stream = await this.streamService.findOne(params.id);

    if (!stream) {
      throw new NotFoundException(`Stream with id: ${params.id} does not exist`);
    }

    return stream;
  }

  @UseGuards(
    AuthGuard('jwt'),
    UserSchoolExistsGuard,
  )
  @Patch(':id')
  async update(@Param() params: FindOneParams, @Req() req, @Body() updateStreamDto: UpdateStreamDto) {
    return await this.streamService.update(params.id, updateStreamDto);
  }

  @UseGuards(
    AuthGuard('jwt'),
    UserSchoolExistsGuard,
  )
  @Delete(':id')
  async remove(@Param() params: FindOneParams, @Req() req) {
    return await this.streamService.remove(params.id);
  }
}
