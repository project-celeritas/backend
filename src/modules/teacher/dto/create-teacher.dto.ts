import { IsNotEmpty, IsNumber, IsNumberString, isArray } from "class-validator";

export class CreateTeacherDto {
  @IsNotEmpty()
  readonly name: string;

  @IsNotEmpty({
    each: true,
  })
  // @IsNumberString()
  // @isArray()
  readonly subjectIds: string[];
  
  @IsNotEmpty({
    each: true,
  })
  // @IsNumberString()
  // @isArray()
  readonly formIds: string[];
  
  @IsNumber()
  readonly schoolId: string;
}
