import { BadRequestException, CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';

import { SubjectService } from 'src/modules/subject/subject.service';

@Injectable()
export class UniqueSchoolSubjectGuard implements CanActivate {
  constructor(private readonly subjectService: SubjectService) { }

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    return this.validateRequest(request);
  }

  async validateRequest(request) {
    const userSchoolId = request.body.schoolId;
    const subjectName = request.body.name;

    const foundSubject = await this.subjectService.findOneByNameAndSchoolId(subjectName, userSchoolId);
    if (foundSubject) {
      throw new BadRequestException(`Subject with name: ${foundSubject.name} already exists in your school`);
    }

    return true;
  }
}
