import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Sequelize } from 'sequelize-typescript';

import { CreateTeacherDto } from './dto/create-teacher.dto';
import { UpdateTeacherDto } from './dto/update-teacher.dto';
import { Teacher } from './models/teacher.model';
import { SubjectTeacher } from '../subject-teacher/models/subject-teacher.model';
import { Subject } from '../subject/models/subject.model';
import { FormTeacher } from '../form-teacher/models/form-teacher.model';
import { Form } from '../form/models/form.model';

@Injectable()
export class TeacherService {
  constructor(
    private sequelize: Sequelize,
    @InjectModel(Teacher)
    private teacherModel: typeof Teacher,
    @InjectModel(SubjectTeacher)
    private subjectTeacherModel: typeof SubjectTeacher,
    @InjectModel(FormTeacher)
    private formTeacherModel: typeof FormTeacher,
  ) { }

  async create(createTeacherDto: CreateTeacherDto) {
    try {
      let createdTeacher: Teacher;

      await this.sequelize.transaction(async t => {
        const transactionHost = { transaction: t };

        // adding a teacher
        createdTeacher = await this.teacherModel.create(createTeacherDto, {
          fields: ['name', 'schoolId'],
          ...transactionHost,
        });

        // adding a record to the subject-teacher table 
        for (const subjectId of createTeacherDto.subjectIds) {
          const createdSubjectTeacher = await this.subjectTeacherModel.create({
            subjectId,
            teacherId: createdTeacher.id
          }, {
            fields: ['subjectId', 'teacherId'],
            ...transactionHost,
          });
        }
        // adding a record to the form-teacher table
        for (const formId of createTeacherDto.formIds) {
          const createdFormTeacher = await this.formTeacherModel.create({
            formId,
            teacherId: createdTeacher.id
          }, {
            fields: ['formId', 'teacherId'],
            ...transactionHost,
          });
        }
      });
      return createdTeacher;

    } catch (error) {
      throw error;
    }
  }

  async findAll(): Promise<Teacher[]> {
    try {
      const foundRecords = await this.teacherModel.findAll({
        attributes: {
          exclude: ['schoolId', 'createdAt', 'updatedAt'],
        },
      });
      return foundRecords;
    } catch (error) {
      throw error;
    }
  }

  async findAllBySchoolId(schoolId: number): Promise<Teacher[]> {
    try {
      const foundRecords = await this.teacherModel.findAll({
        where: {
          schoolId,
        },
        attributes: {
          exclude: ['schoolId', 'createdAt', 'updatedAt'],
        },
      });
      return foundRecords;
    } catch (error) {
      throw error;
    }
  }

  async findAllTeacherSubjects(id: number): Promise<SubjectTeacher[]> {
    try {
      const foundRecords = await this.subjectTeacherModel.findAll({
        attributes: {
          exclude: ['subjectId', 'createdAt', 'updatedAt'],
        },
        where: {
          teacherId: id,
        },
        include: [
          {
            model: Subject,
            attributes: {
              exclude: ['teacherId', 'createdAt', 'updatedAt'],
            },
          }
        ],
      });
      return foundRecords;
    } catch (error) {
      throw error;
    }
  }

  async findAllTeacherForms(id: number): Promise<FormTeacher[]> {
    try {
      const foundRecords = await this.formTeacherModel.findAll({
        attributes: {
          exclude: ['formId', 'createdAt', 'updatedAt'],
        },
        where: {
          teacherId: id,
        },
        include: [
          {
            model: Form,
            attributes: {
              exclude: ['teacherId', 'createdAt', 'updatedAt'],
            },
          }
        ],
      });
      return foundRecords;
    } catch (error) {
      throw error;
    }
  }

  async findOne(id: number): Promise<Teacher> {
    try {
      const foundRecord = await this.teacherModel.findOne({
        where: {
          id,
        },
        attributes: {
          exclude: ['schoolId', 'createdAt', 'updatedAt'],
        },
      });
      return foundRecord;
    } catch (error) {
      throw error;
    }
  }

  async findOneByNameAndSchoolId(name: string, schoolId: number): Promise<Teacher> {
    try {
      const foundRecord = await this.teacherModel.findOne({
        where: {
          schoolId,
          name
        },
        attributes: {
          exclude: ['schoolId', 'createdAt', 'updatedAt'],
        },
      });
      return foundRecord;
    } catch (error) {
      throw error;
    }
  }

  // TODO: add functionality
  async update(id: number, updateTeacherDto: UpdateTeacherDto) {
    return await `This action updates a #${id} teacher`;
  }

  // TODO: add functionality
  async remove(id: number) {
    return await `This action removes a #${id} teacher`;
  }
}
