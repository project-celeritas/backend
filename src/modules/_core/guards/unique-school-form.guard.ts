import { BadRequestException, CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';

import { FormService } from 'src/modules/form/form.service';

@Injectable()
export class UniqueSchoolFormGuard implements CanActivate {
  constructor(private readonly formService: FormService) { }

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    return this.validateRequest(request);
  }

  async validateRequest(request) {
    const userSchoolId = request.body.schoolId;
    const formName = request.body.name;

    const foundForm = await this.formService.findOneByNameAndSchoolId(formName, userSchoolId);
    if (foundForm) {
      throw new BadRequestException(`Form with name: ${foundForm.name} already exists in your school`);
    }

    return true;
  }
}
