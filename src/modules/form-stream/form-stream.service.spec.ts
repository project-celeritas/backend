import { Test, TestingModule } from '@nestjs/testing';
import { FormStreamService } from './form-stream.service';

describe('FormStreamService', () => {
  let service: FormStreamService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FormStreamService],
    }).compile();

    service = module.get<FormStreamService>(FormStreamService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
