import { BelongsTo, Column, DataType, ForeignKey, HasMany, Model, Table } from 'sequelize-typescript';

import { Form } from 'src/modules/form/models/form.model';
import { Stream } from 'src/modules/stream/models/stream.model';
import { Subject } from 'src/modules/subject/models/subject.model';
import { Teacher } from 'src/modules/teacher/models/teacher.model';
import { User } from 'src/modules/user/models/user.model';

@Table
export class School extends Model {
  @Column({
    type: DataType.STRING,
  })
  name: string;

  @Column({
    type: DataType.STRING,
  })
  email: string;

  @Column({
    type: DataType.DATE,
  })
  createdAt: Date;

  @Column({
    type: DataType.DATE,
  })
  updatedAt: Date;

  /* model associations */
  @ForeignKey(() => User)
  @Column({
    type: DataType.INTEGER,
  })
  userId: number;

  @BelongsTo(() => User, 'userId')
  user: User;

  @HasMany(() => Teacher, 'schoolId')
  teachers: Teacher[];
  
  @HasMany(() => Form, 'schoolId')
  forms: Form[];
  
  @HasMany(() => Subject, 'schoolId')
  subjects: Subject[];

  @HasMany(() => Stream, 'schoolId')
  streams: Stream[];
}
