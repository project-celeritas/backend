import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';

import { StreamService } from './stream.service';
import { StreamController } from './stream.controller';
import { Stream } from './models/stream.model';
import { SchoolModule } from '../school/school.module';
import { SchoolService } from '../school/school.service';

@Module({
  imports: [
    SequelizeModule.forFeature([Stream]),
    SchoolModule,
  ],
  controllers: [StreamController],
  providers: [StreamService, SchoolService],
  exports: [SequelizeModule],
})
export class StreamModule {}
