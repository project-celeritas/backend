import { BelongsTo, Column, DataType, ForeignKey, Model, Table } from 'sequelize-typescript';

import { Subject } from 'src/modules/subject/models/subject.model';
import { Teacher } from 'src/modules/teacher/models/teacher.model';

@Table
export class SubjectTeacher extends Model {
  @ForeignKey(() => Subject)
  @Column({
    type: DataType.INTEGER,
  })
  subjectId: number;

  @ForeignKey(() => Teacher)
  @Column({
    type: DataType.INTEGER,
  })
  teacherId: number;

  @BelongsTo(() => Subject, 'subjectId')
  subject: Subject;

  @BelongsTo(() => Teacher, 'teacherId')
  teacher: Teacher;
}
