import { BelongsTo, BelongsToMany, Column, DataType, ForeignKey, Model, Table } from 'sequelize-typescript';

import { SubjectTeacher } from 'src/modules/subject-teacher/models/subject-teacher.model';
import { Teacher } from 'src/modules/teacher/models/teacher.model';
import { School } from 'src/modules/school/models/school.model';

@Table
export class Subject extends Model {
  @Column({
    type: DataType.STRING
  })
  name: string;

  /* model associations */
  @ForeignKey(() => School)
  @Column({
    type: DataType.INTEGER,
  })
  schoolId: number;
  
  @BelongsToMany(() => Teacher, () => SubjectTeacher, 'teacherId')
  teachers: Array<Teacher & {SubjectTeacher: SubjectTeacher}>;

  @BelongsTo(() => School, 'schoolId')
  school: School;

}
