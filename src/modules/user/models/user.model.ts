import {
  AllowNull,
  Column,
  DataType,
  HasOne,
  Model,
  Table,
  Unique,
} from 'sequelize-typescript';
import { School } from 'src/modules/school/models/school.model';

@Table
export class User extends Model {
  // TODO: add column validations
  @Column({
    type: DataType.STRING,
  })
  username: string;

  @AllowNull(false)
  @Unique(true)
  @Column({
    type: DataType.STRING,
  })
  email: string;

  @AllowNull(false)
  @Column({
    type: DataType.STRING,
  })
  password: string;

  @Column({
    type: DataType.BOOLEAN,
    defaultValue: false,
  })
  isAdmin: string;

  @Column({
    type: DataType.DATE,
  })
  createdAt: Date;
  
  @Column({
    type: DataType.DATE,
  })
  updatedAt: Date;

  /* model associations */
  @HasOne(() => School, 'userId')
  school: School;
}
