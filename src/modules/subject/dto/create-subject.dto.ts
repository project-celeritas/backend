import { IsNotEmpty, IsNumber } from "class-validator";

export class CreateSubjectDto {
  @IsNotEmpty()
  readonly name: string;

  @IsNumber()
  readonly schoolId: string;
}
