import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';

import { CreateStreamDto } from './dto/create-stream.dto';
import { UpdateStreamDto } from './dto/update-stream.dto';
import { Stream } from './models/stream.model';

@Injectable()
export class StreamService {
  constructor(
    @InjectModel(Stream)
    private streamModel: typeof Stream,
  ) { }

  async create(createStreamDto: CreateStreamDto) {
    try {
      const createdRecord = await this.streamModel.create(createStreamDto, {
        fields: ['name', 'schoolId'],
      });
      return createdRecord;
    } catch (error) {
      throw error;
    }
  }

  async findAll() {
    try {
      const foundRecords = await this.streamModel.findAll({
        attributes: {
          exclude: ['createdAt', 'updatedAt'],
        },
      });
      return foundRecords;
    } catch (error) {
      throw error;
    }
  }

  async findAllBySchoolId(schoolId: number) {
    try {
      const foundRecords = await this.streamModel.findAll({
        where: {
          schoolId,
        },
        attributes: {
          exclude: ['createdAt', 'updatedAt'],
        },
      });
      return foundRecords;
    } catch (error) {
      throw error;
    }
  }

  async findOne(id: number) {
    try {
      const foundRecord = await this.streamModel.findOne({
        where: {
          id,
        },
        attributes: {
          exclude: ['createdAt', 'updatedAt'],
        },
      });
      return foundRecord;
    } catch (error) {
      throw error;
    }
  }

  async findOneByNameAndSchoolId(name: string, schoolId: number) {
    try {
      const foundRecord = await this.streamModel.findOne({
        where: {
          schoolId,
          name,
        },
        attributes: {
          exclude: ['createdAt', 'updatedAt'],
        },
      });
      return foundRecord;
    } catch (error) {
      throw error;
    }
  }

  async findOneByStreamAndSchoolId(streamId: number, schoolId: number) {
    try {
      const foundRecord = await this.streamModel.findOne({
        where: {
          id: streamId,
          schoolId,
        },
        attributes: {
          exclude: ['createdAt', 'updatedAt'],
        },
      });
      return foundRecord;
    } catch (error) {
      throw error;
    }
  }

  async update(id: number, updateStreamDto: UpdateStreamDto) {
    return `This action updates a #${id} stream`;
  }

  async remove(id: number) {
    return `This action removes a #${id} stream`;
  }
}
