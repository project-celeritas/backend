import { PartialType } from '@nestjs/mapped-types';
import { CreateFormTeacherDto } from './create-form-teacher.dto';

export class UpdateFormTeacherDto extends PartialType(CreateFormTeacherDto) {}
