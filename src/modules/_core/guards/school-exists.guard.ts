import { CanActivate, ExecutionContext, Injectable, ForbiddenException } from '@nestjs/common';
import { Observable } from 'rxjs';
import { SchoolService } from 'src/modules/school/school.service';

@Injectable()
export class SchoolExistsGuard implements CanActivate {
  constructor(private readonly schoolService: SchoolService) { }

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    return this.validateRequest(request);
  }

  async validateRequest(request) {
    const userExist = await this.schoolService.findOneByEmail(request.body.email);
    if (userExist) {
      throw new ForbiddenException('A school with this email already exists');
    }
    return true;
  }
}