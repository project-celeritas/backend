import { PartialType } from '@nestjs/mapped-types';
import { CreateFormStreamDto } from './create-form-stream.dto';

export class UpdateFormStreamDto extends PartialType(CreateFormStreamDto) {}
