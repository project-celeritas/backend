import { CanActivate, ExecutionContext, Injectable, ForbiddenException } from '@nestjs/common';
import { Observable } from 'rxjs';

import { StreamService } from 'src/modules/stream/stream.service';

@Injectable()
export class StreamExistGuard implements CanActivate {
  constructor(
    private readonly streamService: StreamService,
  ) { }

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    return this.validateRequest(request);
  }

  async validateRequest(request) {
    const userSchoolId = request.user.schoolId;
    const streamIds = request.body.streamIds;

    if (streamIds) {
      for (const streamId of streamIds) {
        const subjectExists = await this.streamService.findOneByStreamAndSchoolId(streamId, userSchoolId);
        if (!subjectExists) {
          throw new ForbiddenException(`Stream with id: ${streamId} does not exist in your school`);
        }
      }
    }

    return true;
  }
}