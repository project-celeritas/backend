import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Request,
  NotFoundException,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { TeacherService } from './teacher.service';
import { CreateTeacherDto, UpdateTeacherDto } from './dto';
import { FindOneParams } from 'src/utils/dto/find-one-params';
import { UniqueSchoolGuard } from '../_core/guards/unique-school.guard';
import { UserSchoolExistsGuard } from '../_core/guards/user-school-exists.guard';
import { SchoolSubjectsExistGuard } from '../_core/guards/school-subjects-exist.guard';
import { SchoolFormsExistGuard } from '../_core/guards/school-forms-exist.guard';
import { UniqueSchoolTeacherGuard } from '../_core/guards/unique-school-teacher.guard';

@Controller('teachers')
export class TeacherController {
  constructor(private readonly teacherService: TeacherService) { }

  @UseGuards(
    AuthGuard('jwt'),
    UserSchoolExistsGuard,
    UniqueSchoolGuard, // TODO: investigate whether this guard is necessary
    UniqueSchoolTeacherGuard,
    SchoolSubjectsExistGuard,
    SchoolFormsExistGuard,
  )
  @Post()
  async create(@Request() req, @Body() createTeacherDto: CreateTeacherDto) {
    return await this.teacherService.create(createTeacherDto);
  }

  @UseGuards(
    AuthGuard('jwt'),
    UserSchoolExistsGuard,
  )
  @Get()
  async findAll(@Request() req) {
    return await this.teacherService.findAllBySchoolId(req.user.schoolId);
  }

  @UseGuards(
    AuthGuard('jwt'),
    UserSchoolExistsGuard,
  )
  @Get(':id/subjects')
  async findAllTeacherSubjects(@Param() params: FindOneParams, @Request() req) {
    return await this.teacherService.findAllTeacherSubjects(params.id);
  }

  @UseGuards(
    AuthGuard('jwt'),
    UserSchoolExistsGuard,
  )
  @Get(':id/forms')
  async findAllTeacherForms(@Param() params: FindOneParams, @Request() req) {
    return await this.teacherService.findAllTeacherForms(params.id);
  }

  @UseGuards(
    AuthGuard('jwt'),
    UserSchoolExistsGuard,
  )
  @Get(':id')
  async findOne(@Param() params: FindOneParams, @Request() req) {
    const teacher = await this.teacherService.findOne(params.id);

    if (!teacher) {
      throw new NotFoundException(`Teacher with id: ${params.id} does not exist`);
    }

    return teacher;
  }

  @UseGuards(
    AuthGuard('jwt'),
    UserSchoolExistsGuard,
  )
  @Patch(':id')
  async update(@Param() params: FindOneParams, @Request() req, @Body() updateTeacherDto: UpdateTeacherDto) {
    return await this.teacherService.update(params.id, updateTeacherDto);
  }

  @UseGuards(
    AuthGuard('jwt'),
    UserSchoolExistsGuard,
  )
  @Delete(':id')
  async remove(@Param() params: FindOneParams, @Request() req) {
    return await this.teacherService.remove(params.id);
  }
}
