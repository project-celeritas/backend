export class LogoutUserDto {
  readonly refreshToken: string;
}