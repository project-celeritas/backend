import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, NotFoundException, Req } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { SubjectService } from './subject.service';
import { CreateSubjectDto } from './dto/create-subject.dto';
import { UpdateSubjectDto } from './dto/update-subject.dto';
import { FindOneParams } from 'src/utils/dto/find-one-params';
import { UserSchoolExistsGuard } from '../_core/guards/user-school-exists.guard';
import { UniqueSchoolSubjectGuard } from '../_core/guards/unique-school-subject.guard';

@Controller('subjects')
export class SubjectController {
  constructor(private readonly subjectService: SubjectService) {}

  @UseGuards(
    AuthGuard('jwt'),
    UserSchoolExistsGuard,
    UniqueSchoolSubjectGuard,
  )
  @Post()
  async create(@Body() createSubjectDto: CreateSubjectDto) {
    return await this.subjectService.create(createSubjectDto);
  }

  @UseGuards(
    AuthGuard('jwt'),
    UserSchoolExistsGuard,
  )
  @Get()
  async findAll(@Req() req) {
    return await this.subjectService.findAllBySchoolId(req.user.schoolId);
  }

  @UseGuards(
    AuthGuard('jwt'),
    UserSchoolExistsGuard,
  )
  @Get(':id')
  async findOne(@Param() params: FindOneParams, @Req() req) {
    const subject = await this.subjectService.findOne(params.id);

    if (!subject) {
      throw new NotFoundException(`Subject with id: ${params.id} does not exist`);
    }

    return subject;
  }

  @UseGuards(
    AuthGuard('jwt'),
    UserSchoolExistsGuard,
  )
  @Patch(':id')
  async update(@Param() params: FindOneParams, @Req() req, @Body() updateSubjectDto: UpdateSubjectDto) {
    return await this.subjectService.update(params.id, updateSubjectDto);
  }

  @UseGuards(
    AuthGuard('jwt'),
    UserSchoolExistsGuard,
  )
  @Delete(':id')
  async remove(@Param() params: FindOneParams, @Req() req) {
    return await this.subjectService.remove(params.id);
  }
}
