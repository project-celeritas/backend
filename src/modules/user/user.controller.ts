import {
  Controller,
  Get,
  Body,
  Patch,
  Param,
  Delete,
  NotFoundException,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { UserService } from './user.service';
import { UpdateUserDto } from './dto/update-user.dto';
import { FindOneParams } from 'src/utils/dto/find-one-params';
import { AdminGuard } from '../_core/guards/admin.guard';

@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) { }

  @UseGuards(
    AuthGuard('jwt'),
    AdminGuard,
  )
  @Get()
  async findAll() {
    return await this.userService.findAll();
  }

  // TODO: add guard to ensure a user does not retrieve another user's profile (except for Admin Users)
  @UseGuards(AuthGuard('jwt'))
  @Get(':id')
  async findOne(@Param() params: FindOneParams) {
    const user = await this.userService.findOne(params.id);

    if (!user) {
      throw new NotFoundException(`User with id: ${params.id} does not exist`);
    }

    return user;
  }

  // TODO: add guard to ensure a user does not update another user's profile (except for Admin Users)
  @UseGuards(AuthGuard('jwt'))
  @Patch(':id')
  async update(@Param() params: FindOneParams, @Body() updateUserDto: UpdateUserDto) {
    return await this.userService.update(params.id, updateUserDto);
  }

  // TODO: add guard to ensure a user does not delete another user's profile (except for Admin Users)
  @UseGuards(AuthGuard('jwt'))
  @Delete(':id')
  async remove(@Param() params: FindOneParams) {
    return await this.userService.remove(params.id);
  }
}
