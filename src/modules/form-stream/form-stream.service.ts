import { Injectable } from '@nestjs/common';
import { CreateFormStreamDto } from './dto/create-form-stream.dto';
import { UpdateFormStreamDto } from './dto/update-form-stream.dto';

@Injectable()
export class FormStreamService {
  create(createFormStreamDto: CreateFormStreamDto) {
    return 'This action adds a new formStream';
  }

  findAll() {
    return `This action returns all formStream`;
  }

  findOne(id: number) {
    return `This action returns a #${id} formStream`;
  }

  update(id: number, updateFormStreamDto: UpdateFormStreamDto) {
    return `This action updates a #${id} formStream`;
  }

  remove(id: number) {
    return `This action removes a #${id} formStream`;
  }
}
