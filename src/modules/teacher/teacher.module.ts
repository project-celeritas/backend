import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';

import { TeacherService } from './teacher.service';
import { TeacherController } from './teacher.controller';
import { Teacher } from './models/teacher.model';
import { SchoolService } from '../school/school.service';
import { SchoolModule } from '../school/school.module';
import { SubjectTeacherService } from '../subject-teacher/subject-teacher.service';
import { SubjectTeacherModule } from '../subject-teacher/subject-teacher.module';
import { SubjectModule } from '../subject/subject.module';
import { SubjectService } from '../subject/subject.service';
import { FormTeacherModule } from '../form-teacher/form-teacher.module';
import { FormTeacherService } from '../form-teacher/form-teacher.service';
import { FormModule } from '../form/form.module';
import { FormService } from '../form/form.service';
import { FormStreamModule } from '../form-stream/form-stream.module';
import { FormStreamService } from '../form-stream/form-stream.service';

@Module({
  imports: [
    SequelizeModule.forFeature([Teacher]),
    SchoolModule,
    SubjectModule,
    SubjectTeacherModule,
    FormModule,
    FormTeacherModule,
    FormStreamModule,
  ],
  controllers: [TeacherController],
  providers: [
    TeacherService,
    SchoolService,
    SubjectTeacherService,
    SubjectService,
    FormTeacherService,
    FormService,
    FormStreamService,
  ],
  exports: [SequelizeModule],
})
export class TeacherModule { }
