import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { School } from '../school/models/school.model';

import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './models/user.model';

@Injectable()
export class UserService {
  constructor(
    @InjectModel(User)
    private userModel: typeof User,
  ) {}

  async create(createUserDto: CreateUserDto) {
    try {
      const createdRecord = await this.userModel.create(createUserDto, {
        fields: ['username', 'email', 'password'],
      });
      return createdRecord;
    } catch (error) {
      throw error;
    }
  }

  async findAll() {
    try {
      const foundRecords = await this.userModel.findAll({
        attributes: {
          exclude: ['password', 'createdAt', 'updatedAt', 'schoolId'],
        },
        include: [
          {
            model: School,
            attributes: {
              exclude: ['createdAt', 'updatedAt', 'userId'],
            },
          }
        ],
      });
      return foundRecords;
    } catch (error) {
      throw error;
    }
  }

  async findOne(id: number) {
    try {
      const foundRecord = await this.userModel.findOne({
        where: {
          id,
        },
        attributes: {
          exclude: ['password', 'createdAt', 'updatedAt', 'schoolId'],
        },
        include: [
          {
            model: School,
            attributes: {
              exclude: ['createdAt', 'updatedAt', 'userId'],
            },
          }
        ],
      });
      return foundRecord;
    } catch (error) {
      throw error;
    }
  }

  async findOneByEmail(email: string): Promise<User> {
    try {
      const foundRecord = await this.userModel.findOne({
        where: {
          email,
        },
      });
      return foundRecord;
    } catch (error) {
      throw error;
    }
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    return `This action updates a #${id} user`;
  }

  async remove(id: number) {
    return `This action removes a #${id} user`;
  }
}
