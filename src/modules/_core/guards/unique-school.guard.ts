import { BadRequestException, CanActivate, ExecutionContext, Injectable, NotFoundException } from '@nestjs/common';
import { Observable } from 'rxjs';
import { SchoolService } from 'src/modules/school/school.service';

@Injectable()
export class UniqueSchoolGuard implements CanActivate {
  constructor(private readonly schoolService: SchoolService) { }

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    return this.validateRequest(request);
  }

  async validateRequest(request) {
    const schoolExists = await this.schoolService.findOne(Number(request.body.schoolId));
    if (!schoolExists) {
      throw new NotFoundException(`School with id: ${request.body.schoolId} does not exist`);
    }

    return true;
  }
}
