import { Test, TestingModule } from '@nestjs/testing';
import { SubjectTeacherController } from './subject-teacher.controller';
import { SubjectTeacherService } from './subject-teacher.service';

describe('SubjectTeacherController', () => {
  let controller: SubjectTeacherController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SubjectTeacherController],
      providers: [SubjectTeacherService],
    }).compile();

    controller = module.get<SubjectTeacherController>(SubjectTeacherController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
