import { BadRequestException, CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';

import { TeacherService } from 'src/modules/teacher/teacher.service';

@Injectable()
export class UniqueSchoolTeacherGuard implements CanActivate {
  constructor(private readonly teacherService: TeacherService) { }

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    return this.validateRequest(request);
  }

  async validateRequest(request) {
    const userSchoolId = request.body.schoolId;
    const teacherName = request.body.name;

    const foundTeacher = await this.teacherService.findOneByNameAndSchoolId(teacherName, userSchoolId);
    if (foundTeacher) {
      throw new BadRequestException(`Teacher with name: ${foundTeacher.name} already exists in your school`);
    }

    return true;
  }
}
