'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn(
      "Subjects",
      "schoolId",
      {
        type: Sequelize.INTEGER,
        references: {  // a Subject belongsTo a School (1:1)
          model: 'Schools',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      }
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn("Subjects", "schoolId");
  }
};
