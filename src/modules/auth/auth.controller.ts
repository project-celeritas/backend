import { Controller, Post, Body, UseGuards, Request } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { CreateUserDto } from '../user/dto/create-user.dto';
import { UserExistsGuard } from '../_core/guards/user-exits.guard';
import { AuthService } from './auth.service';
import { LogoutUserDto } from './dto/logout-user.dto';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) { }

  // TODO: use try/catch in methods

  @UseGuards(UserExistsGuard)
  @Post('signup')
  async signup(@Body() createUserDto: CreateUserDto) {
    return await this.authService.create(createUserDto);
  }

  @UseGuards(AuthGuard('local'))
  @Post('login')
  async login(@Request() req) {
    return await this.authService.login(req.user);
  }

  // TODO: add logout functionality
  @Post('logout')
  async logout(@Body() logoutUserDto: LogoutUserDto) {
    return;
  }
}
