import { BelongsTo, Column, DataType, ForeignKey, Model, Table } from 'sequelize-typescript';

import { Form } from 'src/modules/form/models/form.model';
import { Teacher } from 'src/modules/teacher/models/teacher.model';

@Table
export class FormTeacher extends Model {
  @ForeignKey(() => Form)
  @Column({
    type: DataType.INTEGER,
  })
  formId: number;

  @ForeignKey(() => Teacher)
  @Column({
    type: DataType.INTEGER,
  })
  teacherId: number;

  @BelongsTo(() => Form, 'formId')
  form: Form;

  @BelongsTo(() => Teacher, 'teacherId')
  teacher: Teacher;
}
