import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';

import { CreateSchoolDto } from './dto/create-school.dto';
import { UpdateSchoolDto } from './dto/update-school.dto';
import { School } from './models/school.model';

@Injectable()
export class SchoolService {
  constructor(
    @InjectModel(School)
    private schoolModel: typeof School,
  ) {}

  async create(createSchoolDto: CreateSchoolDto) {
    try {
      const createdRecord = await this.schoolModel.create(createSchoolDto, {
        fields: ['name', 'email', 'userId'],
      });
      return createdRecord;
    } catch (error) {
      throw error;
    }
  }

  async findAll(): Promise<School[]> {
    try {
      const foundRecords = await this.schoolModel.findAll({
        attributes: {
          exclude: ['createdAt', 'updatedAt'],
        },
      });
      return foundRecords;
    } catch (error) {
      throw error;
    }
  }

  async findOne(id: number): Promise<School> {
    try {
      const foundRecord = await this.schoolModel.findOne({
        where: {
          id,
        },
        attributes: {
          exclude: ['createdAt', 'updatedAt'],
        },
      });
      return foundRecord;
    } catch (error) {
      throw error;
    }
  }

  async findOneByEmail(email: string): Promise<School> {
    try {
      const foundRecord = await this.schoolModel.findOne({
        where: {
          email,
        },
        attributes: {
          exclude: ['createdAt', 'updatedAt'],
        },
      });
      return foundRecord;
    } catch (error) {
      throw error;
    }
  }

  async findOneByUserId(userId: number): Promise<School> {
    try {
      const foundRecord = await this.schoolModel.findOne({
        where: {
          userId,
        },
        attributes: {
          exclude: ['createdAt', 'updatedAt'],
        },
      });
      return foundRecord;
    } catch (error) {
      throw error;
    }
  }

  // TODO: add functionality
  async update(id: number, updateSchoolDto: UpdateSchoolDto) {
    return `This action updates a #${id} school`;
  }

  // TODO: add functionality
  async remove(id: number) {
    return `This action removes a #${id} school`;
  }
}
