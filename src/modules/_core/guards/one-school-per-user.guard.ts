import { CanActivate, ExecutionContext, Injectable, ForbiddenException } from '@nestjs/common';
import { Observable } from 'rxjs';
import { SchoolService } from 'src/modules/school/school.service';

@Injectable()
export class OneSchoolPerUserGuard implements CanActivate {
  constructor(private readonly schoolService: SchoolService) { }

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    return this.validateRequest(request);
  }

  async validateRequest(request) {
    const foundSchoolRecord = await this.schoolService.findOneByUserId(Number(request.user.id));
    if (foundSchoolRecord) {
      throw new ForbiddenException('This user already has a school');
    }
    return true;
  }
}
