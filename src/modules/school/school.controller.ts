import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Request,
  NotFoundException,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { SchoolService } from './school.service';
import { CreateSchoolDto } from './dto/create-school.dto';
import { UpdateSchoolDto } from './dto/update-school.dto';
import { FindOneParams } from '../../utils/dto/find-one-params';
import { SchoolExistsGuard } from '../_core/guards/school-exists.guard';
import { OneSchoolPerUserGuard } from '../_core/guards/one-school-per-user.guard';
import { AdminGuard } from '../_core/guards/admin.guard';

@Controller('schools')
export class SchoolController {
  constructor(private readonly schoolService: SchoolService) { }

  @UseGuards(AuthGuard('jwt'), SchoolExistsGuard, OneSchoolPerUserGuard)
  @Post()
  async create(@Request() req, @Body() createSchoolDto: CreateSchoolDto) {
    const payload = {
      ...createSchoolDto, 
      userId: req.user.id.toString(), // from JWT token 
    };

    const createdSchool = await this.schoolService.create(payload);
    return createdSchool;
  }

  @UseGuards(
    AuthGuard('jwt'),
    AdminGuard,
  )
  @Get()
  async findAll() {
    return await this.schoolService.findAll();
  }

    // TODO: add guard to ensure a user does not retrieve another school's profile (except for Admin Users)
  @UseGuards(AuthGuard('jwt'))
  @Get(':id')
  async findOne(@Param() params: FindOneParams) {
    const school = await this.schoolService.findOne(params.id);

    if (!school) {
      throw new NotFoundException(`School with id: ${params.id} does not exist`);
    }

    return school;
  }

  // TODO: add guard to ensure a user does not update another school's profile (except for Admin Users)
  @UseGuards(AuthGuard('jwt'))
  @Patch(':id')
  async update(@Param() params: FindOneParams, @Body() updateSchoolDto: UpdateSchoolDto) {
    return await this.schoolService.update(params.id, updateSchoolDto);
  }

  // TODO: add guard to ensure a user does not delete another school's profile (except for Admin Users)
  @UseGuards(AuthGuard('jwt'))
  @Delete(':id')
  async remove(@Param() params: FindOneParams) {
    return await this.schoolService.remove(params.id);
  }
}
