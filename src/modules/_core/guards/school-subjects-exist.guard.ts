import { CanActivate, ExecutionContext, Injectable, ForbiddenException } from '@nestjs/common';
import { Observable } from 'rxjs';
import { SubjectService } from 'src/modules/subject/subject.service';

@Injectable()
export class SchoolSubjectsExistGuard implements CanActivate {
  constructor(private readonly subjectService: SubjectService) { }

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    return this.validateRequest(request);
  }

  async validateRequest(request) {
    const userSchoolId: number = request.user.schoolId;
    const subjectIds = [...new Set<string>(request.body.subjectIds)]; // to remove duplicates

    if (subjectIds) {
      for (const subjectId of subjectIds) {
        const foundSubject = await this.subjectService.findOneByIdAndSchoolId(Number(subjectId), userSchoolId);
        if (!foundSubject) {
          throw new ForbiddenException(`Subject with id: ${subjectId} does not exist in your school`);
        }
      }
    }

    return true;
  }
}