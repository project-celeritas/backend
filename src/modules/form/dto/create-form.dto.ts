import { IsNotEmpty, IsNumber } from "class-validator";

export class CreateFormDto {
  @IsNotEmpty()
  readonly name: string;

  @IsNotEmpty({
    each: true,
  })
  readonly streamIds: string[];

  @IsNumber()
  readonly schoolId: string;
}
