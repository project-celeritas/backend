import { IsNotEmpty, IsNumber } from "class-validator";

export class CreateStreamDto {
  @IsNotEmpty()
  readonly name: string;

  @IsNumber()
  readonly schoolId: string;
}
