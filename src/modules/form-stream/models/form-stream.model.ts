import { BelongsTo, Column, DataType, ForeignKey, Model, Table } from 'sequelize-typescript';

import { Form } from 'src/modules/form/models/form.model';
import { Stream } from 'src/modules/stream/models/stream.model';

@Table
export class FormStream extends Model {
  @ForeignKey(() => Form)
  @Column({
    type: DataType.INTEGER,
  })
  formId: number;

  @ForeignKey(() => Stream)
  @Column({
    type: DataType.INTEGER,
  })
  streamId: number;

  @BelongsTo(() => Form, 'formId')
  form: Form;

  @BelongsTo(() => Stream, 'streamId')
  stream: Stream;
}
