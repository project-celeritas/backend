import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';

import { FormTeacherService } from './form-teacher.service';
import { FormTeacherController } from './form-teacher.controller';
import { FormTeacher } from './models/form-teacher.model';

@Module({
  imports: [
    SequelizeModule.forFeature([FormTeacher]),
  ],
  controllers: [FormTeacherController],
  providers: [FormTeacherService],
  exports: [SequelizeModule],
})
export class FormTeacherModule {}
