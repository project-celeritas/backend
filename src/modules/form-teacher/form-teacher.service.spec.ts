import { Test, TestingModule } from '@nestjs/testing';
import { FormTeacherService } from './form-teacher.service';

describe('FormTeacherService', () => {
  let service: FormTeacherService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FormTeacherService],
    }).compile();

    service = module.get<FormTeacherService>(FormTeacherService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
