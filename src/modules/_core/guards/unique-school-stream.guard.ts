import { BadRequestException, CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';

import { StreamService } from 'src/modules/stream/stream.service';

@Injectable()
export class UniqueSchoolStreamGuard implements CanActivate {
  constructor(private readonly streamService: StreamService) { }

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    return this.validateRequest(request);
  }

  async validateRequest(request) {
    const userSchoolId: number = request.user.schoolId;
    const streamName: string = request.body.name;

    const foundStream = await this.streamService.findOneByNameAndSchoolId(streamName, userSchoolId);
    if (foundStream) {
      throw new BadRequestException(`Stream with name: ${foundStream.name} already exists in your school`);
    }

    return true;
  }
}
