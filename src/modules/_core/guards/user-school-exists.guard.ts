import { BadRequestException, CanActivate, ExecutionContext, Injectable, NotFoundException } from '@nestjs/common';
import { Observable } from 'rxjs';
import { SchoolService } from 'src/modules/school/school.service';

@Injectable()
export class UserSchoolExistsGuard implements CanActivate {
  constructor(private readonly schoolService: SchoolService) { }

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    return this.validateRequest(request);
  }

  async validateRequest(request) {
    const userSchool = await this.schoolService.findOneByUserId(Number(request.user.id));
    if (!userSchool) {
      throw new NotFoundException(`The user has no school`);
    }

    request.user.schoolId = userSchool.id; // to be used by other guards, controllers etc
    request.body.schoolId = userSchool.id; // to be used by DTOs

    return true;
  }
}
