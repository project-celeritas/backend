import { BelongsTo, BelongsToMany, Column, DataType, ForeignKey, Model, Table } from 'sequelize-typescript';

import { School } from 'src/modules/school/models/school.model';
import { Teacher } from 'src/modules/teacher/models/teacher.model';
import { FormTeacher } from 'src/modules/form-teacher/models/form-teacher.model';
import { Stream } from 'src/modules/stream/models/stream.model';
import { FormStream } from 'src/modules/form-stream/models/form-stream.model';

@Table
export class Form extends Model {
  @Column({
    type: DataType.STRING
  })
  name: string;

  /* model associations */
  @ForeignKey(() => School)
  @Column({
    type: DataType.INTEGER,
  })
  schoolId: number;

  @BelongsTo(() => School, 'schoolId')
  school: School;

  @BelongsToMany(() => Teacher, () => FormTeacher, 'teacherId')
  teachers: Array<Teacher & {FormTeacher: FormTeacher}>;

  @BelongsToMany(() => Stream, () => FormStream, 'streamId')
  streams: Array<Stream & {FormStream: FormStream}>;
}
