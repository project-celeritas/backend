<h1 id="top" align="center">Timetable Generator</h1>

<p align="center">

  <a href="https://gitlab.com/project-celeritas/backend/-/commits/main">
    <img alt="pipeline status" src="https://gitlab.com/project-celeritas/backend/badges/main/pipeline.svg" />
  </a>

  <a href="https://gitlab.com/project-celeritas/backend/-/commits/main">
    <img alt="coverage report" src="https://gitlab.com/project-celeritas/backend/badges/main/coverage.svg" />
  </a>

  <!-- <img alt="Date" src="https://img.shields.io/date/19878667" /> -->

  <a alt="license" href="./LICENSE.md">
    <img alt="license" src="https://img.shields.io/badge/license-MIT-1119FF">
  </a>

  <a alt="contributors">
    <img alt="contributors" src="https://img.shields.io/static/v1?label=contributors&message=1&color=brightgreen" />
  </a>

</p>

<hr />

## About
This is a backend application that provides REST APIs for automatic timetable generation for schools. It is built mainly for <a href="https://gitlab.com/project-celeritas/frontend" target="_blank">this frontend SPA</a> client.

## Technologies
The following tools were used in this project:
- [Node.js](https://nodejs.org/en/)
- [Nestjs](https://nestjs.com/)
- [PostgreSQL](https://www.postgresql.org/)

## Requirements
Before starting, you need to have the following installed in your machine:
- [Node](https://nodejs.org/en/) (version 14.14.x preferred)
- [Git](https://git-scm.com)
- [PostgreSQL](https://www.postgresql.org/download/)
- [Yarn](https://yarnpkg.com/getting-started/install) package manager

## Application Configuration
### Cloning and installing dependencies
```bash
# Clone this project into 'timetable-generator' folder
$ git clone https://gitlab.com/project-celeritas/backend.git timetable-generator

# Access
$ cd timetable-generator

# Install dependencies
$ yarn
```

### Setting Environment variables
Create a `.env` file at the root directory of the already cloned project and populate the file with the data displayed below
**NOTE: Replace the env values with your custom values*
```bash
PORT=YOUR-PORT-NUMBER

# Create your postgreSQL database first, then use your database credentials for the configuration below
DB_HOST=YOUR-DB-HOST
DB_PORT=YOUR-DB-PORT
DB_USERNAME=YOUR-DB-USERNAME
DB_PASSWORD=YOUR-DB-PASSWORD
DB_NAME=YOUR-DB-NAME

# Auth token config
ACCESS_TOKEN_SECRET=YOUR-ACCESS-TOKEN-SECRET
REFRESH_TOKEN_SECRET=YOUR-REFRESH-TOKEN-SECRET
ACCESS_TOKEN_EXPIRY_TIMESPAN=YOUR-ACCESS-TOKEN-EXPIRY-TIMESPAN # eg. 1h, 5h

```

### Starting the Application
```bash
# Run the project
$ yarn start

```

## License
This project is under license from MIT. For more details, see the [LICENSE](LICENSE.md) file.

Made with :heart: by <a href="https://gitlab.com/AdongoJr" target="_blank">Moses Adongo</a>

<a href="https://twitter.com/mosesadongo23" target="_blank">
  <img alt="twitter account" src="https://img.shields.io/twitter/follow/mosesadongo23">
</a>

&#xa0;

<a href="#top">Back to top</a>
