import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { FormStreamService } from './form-stream.service';
import { CreateFormStreamDto } from './dto/create-form-stream.dto';
import { UpdateFormStreamDto } from './dto/update-form-stream.dto';

@Controller('form-stream')
export class FormStreamController {
  constructor(private readonly formStreamService: FormStreamService) {}

  @Post()
  create(@Body() createFormStreamDto: CreateFormStreamDto) {
    return this.formStreamService.create(createFormStreamDto);
  }

  @Get()
  findAll() {
    return this.formStreamService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.formStreamService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateFormStreamDto: UpdateFormStreamDto) {
    return this.formStreamService.update(+id, updateFormStreamDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.formStreamService.remove(+id);
  }
}
