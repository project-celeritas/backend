import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Sequelize } from 'sequelize-typescript';
import { FormStream } from '../form-stream/models/form-stream.model';

import { CreateFormDto } from './dto/create-form.dto';
import { UpdateFormDto } from './dto/update-form.dto';
import { Form } from './models/form.model';

@Injectable()
export class FormService {
  constructor(
    private sequelize: Sequelize,
    @InjectModel(Form)
    private formModel: typeof Form,
    @InjectModel(FormStream)
    private formStreamModel: typeof FormStream,
  ) { }

  async create(createFormDto: CreateFormDto) {
    try {
      let createdForm: Form;

      await this.sequelize.transaction(async t => {
        const transactionHost = { transaction: t };

        // adding a form
        createdForm = await this.formModel.create(
          createFormDto,
          {
            fields: ['name', 'schoolId'],
            ...transactionHost,
          },
        );

        // adding a record to the form-streams table
        for (const streamId of createFormDto.streamIds) {
          const createdFormStream = await this.formStreamModel.create({
            streamId,
            formId: createdForm.id
          }, {
            fields: ['formId', 'streamId'],
            ...transactionHost,
          });
        }

      });

      return createdForm;
    } catch (error) {
      throw error;
    }
  }

  async findAll() {
    try {
      const foundRecords = await this.formModel.findAll({
        attributes: {
          exclude: ['createdAt', 'updatedAt'],
        },
      });
      return foundRecords;
    } catch (error) {
      throw error;
    }
  }

  async findAllBySchoolId(schoolId: number) {
    try {
      const foundRecords = await this.formModel.findAll({
        where: {
          schoolId
        },
        attributes: {
          exclude: ['createdAt', 'updatedAt'],
        },
      });
      return foundRecords;
    } catch (error) {
      throw error;
    }
  }

  async findOneByNameAndSchoolId(name: string, schoolId: number) {
    try {
      const foundRecords = await this.formModel.findOne({
        where: {
          schoolId,
          name
        },
        attributes: {
          exclude: ['createdAt', 'updatedAt'],
        },
      });
      return foundRecords;
    } catch (error) {
      throw error;
    }
  }

  async findOne(id: number) {
    try {
      const foundRecord = await this.formModel.findOne({
        where: {
          id,
        },
        attributes: {
          exclude: ['createdAt', 'updatedAt'],
        },
      });
      return foundRecord;
    } catch (error) {
      throw error;
    }
  }

  async findOneByIdAndSchoolId(id: number, schoolId: number) {
    try {
      const foundRecord = await this.formModel.findOne({
        where: {
          schoolId,
          id,
        },
        attributes: {
          exclude: ['createdAt', 'updatedAt'],
        },
      });
      return foundRecord;
    } catch (error) {
      throw error;
    }
  }

  async update(id: number, updateFormDto: UpdateFormDto) {
    return `This action updates a #${id} form`;
  }

  async remove(id: number) {
    return `This action removes a #${id} form`;
  }
}
