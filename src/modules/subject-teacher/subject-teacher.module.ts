import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';

import { SubjectTeacherService } from './subject-teacher.service';
import { SubjectTeacherController } from './subject-teacher.controller';
import { SubjectTeacher } from './models/subject-teacher.model';

@Module({
  imports: [
    SequelizeModule.forFeature([SubjectTeacher]),
  ],
  controllers: [SubjectTeacherController],
  providers: [SubjectTeacherService],
  exports: [SequelizeModule],
})
export class SubjectTeacherModule { }
