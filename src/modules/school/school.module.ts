import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';

import { SchoolService } from './school.service';
import { SchoolController } from './school.controller';
import { School } from './models/school.model';

@Module({
  imports: [SequelizeModule.forFeature([School])],
  controllers: [SchoolController],
  providers: [SchoolService],
  exports: [SequelizeModule],
})
export class SchoolModule {}
