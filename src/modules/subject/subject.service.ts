import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';

import { CreateSubjectDto } from './dto/create-subject.dto';
import { UpdateSubjectDto } from './dto/update-subject.dto';
import { Subject } from './models/subject.model';

@Injectable()
export class SubjectService {
  constructor(
    @InjectModel(Subject)
    private subjectModel: typeof Subject,
  ) { }

  async create(createSubjectDto: CreateSubjectDto) {
    try {
      const createdRecord = await this.subjectModel.create(createSubjectDto, {
        fields: ['name', 'schoolId'],
      });
      return createdRecord;
    } catch (error) {
      throw error;
    }
  }

  async findAll(): Promise<Subject[]> {
    try {
      const foundRecords = await this.subjectModel.findAll({
        attributes: {
          exclude: ['createdAt', 'updatedAt'],
        },
      });
      return foundRecords;
    } catch (error) {
      throw error;
    }
  }

  async findAllBySchoolId(schoolId: number): Promise<Subject[]> {
    try {
      const foundRecords = await this.subjectModel.findAll({
        where: {
          schoolId
        },
        attributes: {
          exclude: ['createdAt', 'updatedAt'],
        },
      });
      return foundRecords;
    } catch (error) {
      throw error;
    }
  }

  async findOne(id: number): Promise<Subject> {
    try {
      const foundRecord = await this.subjectModel.findOne({
        where: {
          id,
        },
        attributes: {
          exclude: ['createdAt', 'updatedAt'],
        },
      });
      return foundRecord;
    } catch (error) {
      throw error;
    }
  }

  async findOneByIdAndSchoolId(id: number, schoolId: number): Promise<Subject> {
    try {
      const foundRecord = await this.subjectModel.findOne({
        where: {
          schoolId,
          id
        },
        attributes: {
          exclude: ['createdAt', 'updatedAt'],
        },
      });
      return foundRecord;
    } catch (error) {
      throw error;
    }
  }

  async findOneByNameAndSchoolId(name:string, schoolId: number): Promise<Subject> {
    try {
      const foundRecord = await this.subjectModel.findOne({
        where: {
          schoolId,
          name
        },
        attributes: {
          exclude: ['createdAt', 'updatedAt'],
        },
      });
      return foundRecord;
    } catch (error) {
      throw error;
    }
  }

  async update(id: number, updateSubjectDto: UpdateSubjectDto) {
    return await `This action updates a #${id} subject`;
  }

  async remove(id: number) {
    return await `This action removes a #${id} subject`;
  }
}
