import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';

import { SubjectService } from './subject.service';
import { SubjectController } from './subject.controller';
import { Subject } from './models/subject.model';
import { SchoolService } from '../school/school.service';
import { SchoolModule } from '../school/school.module';

@Module({
  imports: [
    SequelizeModule.forFeature([Subject]),
    SchoolModule,
  ],
  controllers: [SubjectController],
  providers: [SubjectService, SchoolService],
  exports: [SequelizeModule],
})
export class SubjectModule {}
