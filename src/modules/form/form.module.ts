import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';

import { FormService } from './form.service';
import { FormController } from './form.controller';
import { Form } from './models/form.model';
import { SchoolModule } from '../school/school.module';
import { SchoolService } from '../school/school.service';
import { StreamModule } from '../stream/stream.module';
import { StreamService } from '../stream/stream.service';
import { FormStreamModule } from '../form-stream/form-stream.module';
import { FormStreamService } from '../form-stream/form-stream.service';

@Module({
  imports: [
    SequelizeModule.forFeature([Form]),
    SchoolModule,
    StreamModule,
    FormStreamModule,
  ],
  controllers: [FormController],
  providers: [FormService, SchoolService, StreamService, FormStreamService],
  exports: [SequelizeModule],
})
export class FormModule {}
