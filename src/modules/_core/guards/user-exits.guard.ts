import { CanActivate, ExecutionContext, Injectable, ForbiddenException } from '@nestjs/common';
import { Observable } from 'rxjs';
import { UserService } from 'src/modules/user/user.service';

@Injectable()
export class UserExistsGuard implements CanActivate {
  constructor(private readonly userService: UserService) { }

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    return this.validateRequest(request);
  }

  async validateRequest(request) {
    const userExists = await this.userService.findOneByEmail(request.body.email);
    if (userExists) {
      throw new ForbiddenException('A user with this email already exists');
    }
    return true;
  }
}