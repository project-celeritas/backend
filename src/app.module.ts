import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { SequelizeModule } from '@nestjs/sequelize';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CoreModule } from './modules/_core/core.module';
import { SchoolModule } from './modules/school/school.module';
import { School } from './modules/school/models/school.model';
import { AuthModule } from './modules/auth/auth.module';
import { UserModule } from './modules/user/user.module';
import { User } from './modules/user/models/user.model';
import { TeacherModule } from './modules/teacher/teacher.module';
import { Teacher } from './modules/teacher/models/teacher.model';
import { SubjectModule } from './modules/subject/subject.module';
import { SubjectTeacherModule } from './modules/subject-teacher/subject-teacher.module';
import { Subject } from './modules/subject/models/subject.model';
import { SubjectTeacher } from './modules/subject-teacher/models/subject-teacher.model';
import { FormModule } from './modules/form/form.module';
import { FormTeacherModule } from './modules/form-teacher/form-teacher.module';
import { StreamModule } from './modules/stream/stream.module';
import { TimetableModule } from './modules/timetable/timetable.module';
import { Form } from './modules/form/models/form.model';
import { FormTeacher } from './modules/form-teacher/models/form-teacher.model';
import { Stream } from './modules/stream/models/stream.model';
import { Timetable } from './modules/timetable/models/timetable.model';
import { FormStreamModule } from './modules/form-stream/form-stream.module';
import { FormStream } from './modules/form-stream/models/form-stream.model';

const ENV = process.env.NODE_ENV;

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: ENV ? `.env.${ENV}` : `.env`,
      isGlobal: true,
    }),
    SequelizeModule.forRoot({
      dialect: 'postgres',
      dialectOptions: {
        ssl: {
          require: true,
          rejectUnauthorized: false,
        },
      },
      ssl: true,
      host: process.env.DB_HOST,
      port: parseInt(process.env.DB_PORT),
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
      models: [
        School, 
        User, 
        Teacher, 
        Subject, 
        SubjectTeacher, 
        Form, 
        FormTeacher,
        Stream,
        FormStream,
        Timetable,
      ],
    }),
    CoreModule,
    AuthModule,
    UserModule,
    SchoolModule,
    TeacherModule,
    SubjectModule,
    SubjectTeacherModule,
    FormModule,
    FormTeacherModule,
    StreamModule,
    FormStreamModule,
    TimetableModule,
    FormStreamModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
