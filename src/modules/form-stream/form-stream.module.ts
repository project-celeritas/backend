import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';

import { FormStreamService } from './form-stream.service';
import { FormStreamController } from './form-stream.controller';
import { FormStream } from './models/form-stream.model';

@Module({
  imports: [
    SequelizeModule.forFeature([FormStream]),
  ],
  controllers: [FormStreamController],
  providers: [FormStreamService],
  exports: [SequelizeModule],
})
export class FormStreamModule {}
