import { IsEmail, IsNotEmpty } from "class-validator";

export class CreateSchoolDto {
  @IsNotEmpty()
  readonly name: string;
  
  @IsNotEmpty()
  @IsEmail()
  readonly email: string;

  readonly userId?: string;
}
