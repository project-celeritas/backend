import { IsEmail, IsNotEmpty, MinLength } from "class-validator";

export class CreateUserDto {
  readonly username: string;

  @IsNotEmpty()
  @IsEmail()
  readonly email: string;

  @IsNotEmpty()
  @MinLength(4)
  readonly password: string;
}
