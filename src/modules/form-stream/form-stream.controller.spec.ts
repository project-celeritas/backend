import { Test, TestingModule } from '@nestjs/testing';
import { FormStreamController } from './form-stream.controller';
import { FormStreamService } from './form-stream.service';

describe('FormStreamController', () => {
  let controller: FormStreamController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FormStreamController],
      providers: [FormStreamService],
    }).compile();

    controller = module.get<FormStreamController>(FormStreamController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
