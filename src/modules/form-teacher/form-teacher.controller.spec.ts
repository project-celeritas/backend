import { Test, TestingModule } from '@nestjs/testing';
import { FormTeacherController } from './form-teacher.controller';
import { FormTeacherService } from './form-teacher.service';

describe('FormTeacherController', () => {
  let controller: FormTeacherController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FormTeacherController],
      providers: [FormTeacherService],
    }).compile();

    controller = module.get<FormTeacherController>(FormTeacherController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
