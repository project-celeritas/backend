import { Injectable } from '@nestjs/common';
import { CreateFormTeacherDto } from './dto/create-form-teacher.dto';
import { UpdateFormTeacherDto } from './dto/update-form-teacher.dto';

@Injectable()
export class FormTeacherService {
  create(createFormTeacherDto: CreateFormTeacherDto) {
    return 'This action adds a new formTeacher';
  }

  findAll() {
    return `This action returns all formTeacher`;
  }

  findOne(id: number) {
    return `This action returns a #${id} formTeacher`;
  }

  update(id: number, updateFormTeacherDto: UpdateFormTeacherDto) {
    return `This action updates a #${id} formTeacher`;
  }

  remove(id: number) {
    return `This action removes a #${id} formTeacher`;
  }
}
